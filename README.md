# The Lambda Calculus

This project aims to provide a simple tool for learning about the lambda calculus. 

## Index
1. How to Run
2. How to Use

## How to Run

Running the `LambdaCalculus` is as easy as pie. Just donwload the release jar from the [releases page](https://git.thm.de/bpfr67/lambda-calculus/-/releases) and run it using a JVM (Java 17 required).

### Alternative #1 - The Easy Way

1. Clone and import the project into your preferred IDE
2. Run the `main`-method in the `LambdaCalculus`-class

### Alternative #2 - The Medium Rare Way

1. Clone the project and switch to the project folder with a shell of your choice
2. Use `maven` to build and run the project.

### Alternative #3 - The Hard Way

1. Clone the project and switch to the project folder with a shell of your choice
2. Compile all java files using `javac`
3. Run the `main`-method using `java` (don't forget to put **all** `*.class`-files to the classpath)

## How to Use

When running, type `:help` into the interactive `REPL` (read-evaluation-print-loop). You can also type arbitrary lambda terms. Just try 😉. 

Tip:
A lambda term is either a variable name, a lambda (e.g. `λ x . x`) or an application of a lambda (just two terms in a sequence, e.g. `(λ x . x) y`). Since it is unlikely that your keyboard offers the greek letter `λ`, you can also type a backslash `\` instead.

## How to Contribute

Feel free to contribute code, feature requests or other cool ideas regarding this project. 
Just use the issue system or create merge requests.
I will review your changes as soon as possible. 
