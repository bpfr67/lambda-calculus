package lc.common;

import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public sealed interface Maybe<T> {

    static <T> Maybe<T> nothing() {
        return Nothing.instance.self();
    }

    static <T> Maybe<T> of(final T value) {
        return new Just<>(value);
    }
    static <T> Maybe<T> ofNull(final T value) {
        return value == null ? Maybe.nothing() : Maybe.of(value);
    }
    <U> Maybe<U> map(final Function<? super T, ? extends U> function);

    <U> Maybe<U> flatMap(final Function<? super T, Maybe<? extends U>> function);

    Maybe<T> or(final Maybe<? extends T> other);

    Maybe<T> filter(final Predicate<? super T> predicate);

    Maybe<T> or(final Supplier<Maybe<? extends T>> other);

    T getOrElse(final Supplier<T> supplier);

    T get();

    boolean exists(final Predicate<? super T> predicate);

    boolean forall(final Predicate<? super T> predicate);

    <U> U fold(final Function<? super T, ? extends U> function, final Supplier<? extends U> zero);

    boolean isEmpty();

    boolean nonEmpty();

    record Just<T>(T value) implements Maybe<T> {

        @Override
        public <U> Maybe<U> map(final Function<? super T, ? extends U> function) {
            return new Just<>(function.apply(value));
        }

        @Override
        public <U> Maybe<U> flatMap(final Function<? super T, Maybe<? extends U>> function) {
            @SuppressWarnings("unchecked")
            final Maybe<U> result = (Maybe<U>) function.apply(value);
            return result;
        }

        @Override
        public Maybe<T> or(final Maybe<? extends T> other) {
            return this;
        }

        @Override
        public Maybe<T> filter(final Predicate<? super T> predicate) {
            return predicate.test(value) ? this : Nothing.instance.self();
        }

        @Override
        public Maybe<T> or(Supplier<Maybe<? extends T>> other) {
            return this;
        }

        @Override
        public T getOrElse(final Supplier<T> supplier) {
            return value;
        }

        @Override
        public T get() {
            return value;
        }

        @Override
        public boolean exists(final Predicate<? super T> predicate) {
            return predicate.test(value);
        }

        @Override
        public boolean forall(final Predicate<? super T> predicate) {
            return predicate.test(value);
        }

        @Override
        public <U> U fold(final Function<? super T, ? extends U> function, final Supplier<? extends U> zero) {
            return function.apply(value);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean nonEmpty() {
            return true;
        }

    }

    record Nothing<T>() implements Maybe<T> {

        private static final Nothing<?> instance = new Nothing<>();

        public <U> Nothing<U> self() {
            @SuppressWarnings("unchecked")
            final Nothing<U> instance = (Nothing<U>) this;
            return instance;
        }

        @Override
        public <U> Maybe<U> map(final Function<? super T, ? extends U> function) {
            return self();
        }

        @Override
        public <U> Maybe<U> flatMap(final Function<? super T, Maybe<? extends U>> function) {
            return self();
        }

        @Override
        public Maybe<T> or(final Maybe<? extends T> other) {
            @SuppressWarnings("unchecked")
            final Maybe<T> result = (Maybe<T>) other;
            return result;
        }

        @Override
        public Maybe<T> filter(final Predicate<? super T> predicate) {
            return this;
        }

        @Override
        public Maybe<T> or(Supplier<Maybe<? extends T>> other) {
            @SuppressWarnings("unchecked")
            final Maybe<T> result = (Maybe<T>) other.get();
            return result;
        }

        @Override
        public T getOrElse(final Supplier<T> supplier) {
            return supplier.get();
        }

        @Override
        public T get() {
            throw new NoSuchElementException();
        }

        @Override
        public boolean exists(final Predicate<? super T> predicate) {
            return false;
        }

        @Override
        public boolean forall(final Predicate<? super T> predicate) {
            return true;
        }

        @Override
        public <U> U fold(final Function<? super T, ? extends U> function, final Supplier<? extends U> zero) {
            return zero.get();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean nonEmpty() {
            return false;
        }

    }

}

