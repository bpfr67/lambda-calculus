package lc.common;

import lc.syntax.ParseException;
import lc.syntax.Parser;
import lc.syntax.tree.Term;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Context implements Iterable<Context.Entry> {

    public final String name;
    public final Maybe<Context> parent;
    private final Map<String, Entry> entries;

    public Context(final Maybe<Context> parent, final String name) {
        this.name = name;
        this.parent = parent;
        this.entries = new HashMap<>();
    }

    public Context(final String name) {
        this(Maybe.nothing(), name);
    }

    public Context insert(final String name, final Term term) {
        return this.insert(name, term, Maybe.nothing());
    }

    public Context insert(final String name, final Term term, final Maybe<String> documentation) {
        this.entries.put(name, new Entry(name, term, documentation));
        save();
        return this;
    }

    public Context remove(final String name) {
        this.entries.remove(name);
        save();
        return this;
    }

    public boolean contains(final String name) {
        return this.entries.containsKey(name);
    }

    public Maybe<Entry> get(final String name) {
        if (contains(name)) {
            return Maybe.of(this.entries.get(name));
        } else {
            return Maybe.nothing();
        }
    }

    public boolean exists(final String name) {
        if (contains(name)) {
            return true;
        } else {
            return parent.exists(context -> context.exists(name));
        }
    }

    public Maybe<Entry> lookup(final String name) {
        if (contains(name)) {
            return Maybe.of(this.entries.get(name));
        } else {
            return parent.flatMap(context -> context.lookup(name));
        }
    }

    private File getFile() throws IOException {
        final File file = Paths.get(
            System.getProperty("user.home"),
            ".lambda-calculus",
            name + ".config"
        ).toAbsolutePath().toFile();

        if (!file.getParentFile().exists() && !file.getParentFile().mkdirs()) {
            throw new RuntimeException("failed to create directory '" + file.getParentFile().getAbsolutePath() + "' for context '" + name + "'");
        }

        if (file.exists()) {
            if (file.isDirectory()) {
                throw new RuntimeException("failed to access context '" + name + "' ('" + file.getAbsolutePath() + "' is a directory)");
            }
        } else if (!file.createNewFile()) {
            throw new RuntimeException("failed to write context '" + name + "'");
        }

        return file;
    }

    public void save() {
        try {
            if (parent.nonEmpty()) {
                parent.get().save();
            }

            final File file = getFile();
            /*if (!(file.delete() && file.createNewFile())) {
                throw new RuntimeException("failed to write context '" + name + "'");
            }*/

            final PrintStream stream = new PrintStream(new FileOutputStream(file));

            for (final Entry entry: this) {
                stream.printf("%s = %s", entry.name, entry.term);
                if (entry.documentation.nonEmpty()) {
                    stream.printf(" # %s", entry.documentation.get());
                }
                stream.println();
            }

            stream.flush();
            stream.close();
        } catch (final IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void load() {
        try {
            if (parent.nonEmpty()) {
                parent.get().load();
            }

            final File file = getFile();
            final Scanner scanner = new Scanner(new FileInputStream(file));

            final Pattern pattern = Pattern.compile("\\s*(\\S+)\\s*=([^#\n]*)(#(.*))?");
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();

                if (line.isEmpty()) {
                    continue;
                }

                final Matcher matcher = pattern.matcher(line);

                if (!matcher.matches()) {
                    throw new RuntimeException("illegal file format for context '" + this.name + "'");
                }

                final String name = matcher.group(1);
                final String termString = matcher.group(2).trim();
                final String documentation = matcher.group(3);

                try {
                    final Term term = Parser.parse(termString);
                    insert(name, term, Maybe.ofNull(documentation).map(s -> s.substring(1)).map(String::trim).filter(Predicate.not(String::isEmpty)));
                } catch (final ParseException exception) {
                    throw new RuntimeException("illegal term for binding '" + name + "' in context '" + this.name + "'");
                }
            }

            scanner.close();
        } catch (final IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public Iterator<Entry> iterator() {
        return this.entries.values().iterator();
    }

    public record Entry(String name, Term term, Maybe<String> documentation) {}

}
