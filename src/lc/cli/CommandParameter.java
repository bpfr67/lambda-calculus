package lc.cli;

public final class CommandParameter {

    public final String name;
    private String description = "";
    private boolean optional = false;

    private boolean term = false;

    public CommandParameter(final String name) {
        this.name = name;
    }

    public String description() {
        return this.description;
    }

    public CommandParameter description(final String description) {
        this.description = description;
        return this;
    }

    public CommandParameter term() {
        this.term = true;
        return this;
    }

    public boolean isTerm() {
        return term;
    }

    public CommandParameter optional() {
        this.optional = true;
        return this;
    }

    public boolean isOptional() {
        return this.optional;
    }

}
