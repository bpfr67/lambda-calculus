package lc.cli;

import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.ptr.PointerByReference;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public abstract class CommandLineInterface {

    private final BufferedReader reader;
    private final BufferedWriter writer;

    private String prompt;

    public CommandLineInterface(final String prompt) {
        if (Platform.isWindows()) {
            final int previousCodePage = Kernel32.INSTANCE.GetConsoleCP();
            if (Kernel32.INSTANCE.SetConsoleCP(65001) && Kernel32.INSTANCE.SetConsoleOutputCP(65001)) {
                System.out.println("[info] Switched to utf-8!");
            } else {
                Kernel32.INSTANCE.SetConsoleCP(previousCodePage);
                Kernel32.INSTANCE.SetConsoleOutputCP(previousCodePage);

                final int errorCode = Kernel32.INSTANCE.GetLastError();

                PointerByReference buffer = new PointerByReference();
                Kernel32.INSTANCE.FormatMessage(Kernel32.INSTANCE.FORMAT_MESSAGE_ALLOCATE_BUFFER | Kernel32.INSTANCE.FORMAT_MESSAGE_FROM_SYSTEM | Kernel32.INSTANCE.FORMAT_MESSAGE_IGNORE_INSERTS, null, errorCode, 0, buffer, 0, null);
                Pointer ptr = buffer.getValue();
                String reason;
                try {
                    String message = ptr.getWideString(0).trim();

                    if (message.charAt(message.length() - 1) == '.') {
                        message = message.substring(0, message.length() - 1);
                    }

                    message = Character.toLowerCase(message.charAt(0)) + message.substring(1);

                    reason = " (" + message + ")";
                } catch (final Throwable throwable) {
                    reason = "";
                } finally {
                    Kernel32.INSTANCE.LocalFree(ptr);
                }

                System.out.printf("[warning] Failed to switch to utf-8%s!\n", reason);
            }
        }

        // do not access console before the charset is set to utf-8
        final Console console = System.console();
        this.prompt = prompt;

        if (console != null) {
            reader = new BufferedReader(console.reader());
            writer = new BufferedWriter(console.writer());
        } else {
            reader = new BufferedReader(new InputStreamReader(System.in));
            writer = new BufferedWriter(new PrintWriter(System.out));
        }
    }

    public final void setPrompt(final String prompt) {
        this.prompt = prompt;
    }

    public final String getPrompt() {
        return prompt;
    }

    public abstract void accept(final String input) throws IOException;

    private String read(final String message, final Object ... objects) throws IOException {
        printf(message, objects);
        return reader.readLine();
    }

    public final void printf(final String message, final Object ... objects) throws IOException {
        writer.write(String.format(message, objects));
        writer.flush();
    }

    public final void prompt() throws IOException {
        final Predicate<CharSequence> isComplete = CommandLineInterface.isComplete(
            Delimiter.RoundBracket,
            Delimiter.QuotationMark,
            Delimiter.CurlyBracket,
            Delimiter.SquareBracket
        );

        boolean exit = false;
        do {
            String next;

            if ((next = read("%s ", prompt)) == null) {
                exit = true;
                break;
            }

            final StringBuilder line = new StringBuilder(next);

            while (!isComplete.test(line)) {
                if ((next = read("%" + prompt.length() + "s ", "|")) == null) {
                    exit = true;
                    break;
                }

                line.append('\n');
                line.append(next);
            }

            accept(line.toString());
        } while (!exit);
    }

    public static Predicate<CharSequence> isComplete(final Delimiter ... delimiters) {
        final List<Delimiter> delimiterList = Arrays.stream(delimiters).distinct().toList();
        final boolean roundBracketsEnabled = delimiterList.contains(Delimiter.RoundBracket);
        final boolean curlyBracketsEnabled = delimiterList.contains(Delimiter.CurlyBracket);
        final boolean squareBracketsEnabled = delimiterList.contains(Delimiter.SquareBracket);
        final boolean quotationMarksEnabled = delimiterList.contains(Delimiter.QuotationMark);

        return input -> {
            final ArrayList<Delimiter> stack = new ArrayList<>();
            boolean quoted = false;

            for (int i = 0; i < input.length(); ++i) {
                switch (input.charAt(i)) {
                    case '(':
                        if (!quoted && roundBracketsEnabled) stack.add(Delimiter.RoundBracket);
                        break;
                    case ')':
                        if (!quoted && roundBracketsEnabled && !stack.isEmpty() && stack.get(stack.size() - 1) == Delimiter.RoundBracket) stack.remove(stack.size() - 1);
                        break;

                    case '{':
                        if (!quoted && curlyBracketsEnabled) stack.add(Delimiter.CurlyBracket);
                        break;
                    case '}':
                        if (!quoted && curlyBracketsEnabled && !stack.isEmpty() && stack.get(stack.size() - 1) == Delimiter.CurlyBracket) stack.remove(stack.size() - 1);
                        break;

                    case '[':
                        if (!quoted && squareBracketsEnabled) stack.add(Delimiter.SquareBracket);
                        break;
                    case ']':
                        if (!quoted && squareBracketsEnabled && !stack.isEmpty() && stack.get(stack.size() - 1) == Delimiter.SquareBracket) stack.remove(stack.size() - 1);
                        break;

                    case '"':
                        if (quoted) {
                            quoted = false;
                        } else {
                            quoted = quotationMarksEnabled;
                        }
                        break;
                }
            }

            return !quoted && stack.isEmpty();
        };
    }

    public enum Delimiter {
        RoundBracket,
        CurlyBracket,
        SquareBracket,
        QuotationMark
    }

}
