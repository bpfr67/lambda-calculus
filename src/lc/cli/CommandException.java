package lc.cli;

public final class CommandException extends Exception {

    public CommandException(final String message) {
        super(message);
    }

}
