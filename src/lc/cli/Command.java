package lc.cli;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public abstract class Command implements Consumer<String[]> {

    public final String name;
    private String description = "";
    private final List<String> synonyms = new ArrayList<>();
    private final List<CommandParameter> parameters = new ArrayList<>();

    public Command(final String name) {
        this.name = name;
    }

    public final String description() {
        return this.description;
    }

    public final Command description(final String description) {
        this.description = description;
        return this;
    }

    public final Command parameters(final CommandParameter... parameters) {
        this.parameters.addAll(Arrays.asList(parameters));
        return this;
    }

    public final List<CommandParameter> parameters() {
        return Collections.unmodifiableList(parameters);
    }

    public final Command synonyms(final String... synonyms) {
        this.synonyms.addAll(Arrays.asList(synonyms));
        return this;
    }

    public final List<String> synonyms() {
        return Collections.unmodifiableList(synonyms);
    }

    public final boolean parse(final String input) throws CommandException, IOException {
        final String[] words = input.trim().split("\\s+");

        if (words.length == 0) {
            return false;
        }

        final String command = words[0];

        if (!command.equalsIgnoreCase(this.name) && this.synonyms.stream().noneMatch(command::equalsIgnoreCase)) {
            return false;
        }

        final Predicate<CharSequence> isComplete = CommandLineInterface.isComplete(CommandLineInterface.Delimiter.RoundBracket, CommandLineInterface.Delimiter.SquareBracket, CommandLineInterface.Delimiter.QuotationMark);

        int wordIndex = 1;
        final List<String> arguments = new ArrayList<>();
        for (int i = 0; i < this.parameters.size(); ++i) {
            final CommandParameter parameter = this.parameters.get(i);

            if (wordIndex >= words.length) {
                if (parameter.isOptional()) {
                    continue;
                } else {
                    throw new CommandException(String.format("Missing argument for %s of command %s", parameter.name, this.name));
                }
            }

            final StringBuilder buffer = new StringBuilder(words[wordIndex++]);
            if (i == this.parameters.size() - 1) {
                // last parameter, accumulate all remaining words
                while (wordIndex < words.length) {
                    buffer.append(' ');
                    buffer.append(words[wordIndex++]);
                }
            } else if (parameter.isTerm()) {
                // accumulate as long as the term is not finished
                while (!isComplete.test(buffer) && wordIndex < words.length) {
                    buffer.append(' ');
                    buffer.append(words[wordIndex++]);
                }

                if (!isComplete.test(buffer)) {
                    throw new CommandException(String.format("Incomplete term provided for parameter %s of command %s", parameter.name, this.name));
                }
            }

            arguments.add(buffer.toString());
        }

        this.execute(arguments.toArray(new String[0]));

        return true;
    }

    @Override
    public final void accept(final String[] arguments) {
        try {
            execute(arguments);
        } catch (final RuntimeException exception) {
            throw exception;
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public abstract void execute(final String[] arguments) throws IOException;

}
