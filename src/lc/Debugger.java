package lc;

import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import lc.cli.Command;
import lc.cli.CommandParameter;
import lc.common.Context;
import lc.reduction.ReductionStrategy;
import lc.syntax.ParseException;
import lc.syntax.Parser;
import lc.syntax.tree.Term;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static lc.LambdaCalculus.cli;

public final class Debugger {

    public static Term term = null;
    public static ReductionStrategy strategy = LambdaCalculus.strategy;

    public static boolean naturals = LambdaCalculus.naturals;

    private Debugger() {}

    public static final Command[] commands = new Command[] {
        new Command("display") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                cli.printf("\n%s\n\n", term.show(Debugger.naturals));
            }

        }.synonyms("d").description("Shows the current term."),

        new Command("naturals") {
            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final String enable = arguments[0].trim().toLowerCase();
                    if (enable.equals("y") || enable.equals("yes") || enable.equals("true") || enable.equals("t") || enable.equals("1")) {
                        Debugger.naturals = true;
                    } else if (enable.equals("n") || enable.equals("no") || enable.equals("false") || enable.equals("f") || enable.equals("0")) {
                        Debugger.naturals = false;
                    } else {
                        cli.printf("[error] Illegal boolean value '%s'\n", enable);
                        return;
                    }
                } else {
                    Debugger.naturals = !Debugger.naturals;
                }

                if (Debugger.naturals) {
                    cli.printf("\nEnabled the automatic conversion between terms and natural numbers.\n\n");
                } else {
                    cli.printf("\nDisabled the automatic conversion between terms and natural numbers.\n\n");
                }
            }
        }.synonyms("nat").description("Enables or disables the automatic conversion between terms and natural numbers.")
            .parameters(new CommandParameter("enable").optional().description("Whether or not the natural number mode should be enabled.")),

        new Command("complete") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                List<String> free = term.free().filter(LambdaCalculus.context::exists).toList();
                while (!free.isEmpty()) {
                    for (final String name: free) {
                        term = term.substitute(name, LambdaCalculus.context.lookup(name).get().term());
                    }
                    free = term.free().filter(LambdaCalculus.context::exists).toList();
                }

                if (naturals) {
                    final List<String> numbers = term.free().filter(variable -> variable.matches("\\d+")).toList();

                    do {
                        for (final String number: numbers) {
                            term = term.substitute(number, Term.fromNumber(Integer.parseInt(number)));
                        }
                    } while (term.free().anyMatch(numbers::contains));
                }

                if (naturals) {
                    final List<String> numbers = term.free().filter(variable -> variable.matches("\\d+")).toList();

                    do {
                        for (final String number: numbers) {
                            term = term.substitute(number, Term.fromNumber(Integer.parseInt(number)));
                        }
                    } while (term.free().anyMatch(numbers::contains));
                }

                cli.printf("\nTerm after introducing all global definitions:\n\n%s\n\n", term.show(Debugger.naturals));
            }

        }.synonyms("c").description("Introduces all global definitions in the current term. That is, all free occurrences of any global binding are replaced by the corresponding definition."),

        new Command("introduce") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final String name = arguments[0];
                final boolean recursive;

                if (arguments.length > 1) {
                    if (arguments[1].equalsIgnoreCase("--recursive")) {
                        recursive = true;
                    } else {
                        cli.printf("[error] Unknown option '%s'\n", arguments[1]);
                        return;
                    }
                } else {
                    recursive = false;
                }

                if (LambdaCalculus.context.contains(name)) {
                    final Context.Entry entry = LambdaCalculus.context.get(name).get();
                    do {
                        term = term.substitute(entry.name(), entry.term());
                    } while (recursive && term.free().anyMatch(name::equals));

                    cli.printf("\nTerm after %sintroducing the global definition '%s':\n\n%s\n\n", recursive ? "recursively " : "", name, term.show(Debugger.naturals));
                } else {
                    cli.printf("[error] There is no global definition with name '%s'\n", name);
                }
            }

        }.synonyms("i")
            .description("Introduces the global definition in the current term that corresponds to the provided name. That is, all free occurrences of the name are substituted once by its corresponding global definition.")
            .parameters(
                new CommandParameter("name").description("The name of the global definition."),
                new CommandParameter("--recursive").optional().description("When provided, the name is substituted as long as it occurs free in the term.")
            ),
        new Command("substitute") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final String name = arguments[0];
                try {
                    final Term term = Parser.parse(arguments[1]);
                    Debugger.term = Debugger.term.substitute(name, term);
                    cli.printf("\nSubstituted %s by '%s':\n\n%s\n\n", name, term.show(Debugger.naturals), Debugger.term.show(Debugger.naturals));
                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception.getMessage());
                }
            }

        }.synonyms("/", "sub").description("Substitutes all free occurrences of the provided variable with the term.").parameters(
            new CommandParameter("variable").description("The variable that should be substituted."),
            new CommandParameter("term").description("The term which should be placed instead of the variable.").term()
        ),
        new Command("free") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final Set<String> fv = term.free().collect(Collectors.toSet());
                cli.printf("\nThe term\n\n%s\n\ncontains %d free variable%s%s\n\n", term.show(Debugger.naturals), fv.size(), fv.size() == 1 ? "" : "s", fv.isEmpty() ? "." : ":\n\n" + String.join(", ", fv));
            }

        }.synonyms("f").description("Searches the current term for all occurrences of free variables and lists them."),
        new Command("reduce") {
            @Override
            public void execute(final String[] arguments) throws IOException {
                final int steps;
                if (arguments.length > 0) {
                    if (arguments[0].trim().matches("\\d+")) {
                        steps = Math.min(1000, Integer.parseInt(arguments[0].trim()));
                    } else {
                        cli.printf("[error] '%s' is not a valid number.\n", arguments[0]);
                        return;
                    }
                } else {
                    steps = 1;
                }

                for (int i = 0; i < steps; ++i) {
                    cli.printf("\nReducing term:\n\n%s\n", term.show(Debugger.naturals));
                    term = strategy.reduce(term);
                }

                cli.printf("\nResult after %d reduction%s:\n\n%s\n", steps, steps == 1 ? "" : "s", term.show(Debugger.naturals));

                if (strategy.isNormal(term)) {
                    cli.printf("\nThe term is in %s!\n", strategy.getNormalFormName());
                }

                cli.printf("\n");
            }

        }.parameters(new CommandParameter("iterations").optional().description("The number of reductions that should be applied (max. 1000 at once).")).synonyms("r").description("Reduces the term by one or more steps."),

        new Command("strategy") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final String strategy = arguments[0];
                    final Optional<ReductionStrategy> option = Arrays.stream(LambdaCalculus.strategies).filter(s -> s.getStrategyName().equalsIgnoreCase(strategy) || s.getStrategyIdentifier().equalsIgnoreCase(strategy)).findFirst();

                    if (option.isEmpty()) {
                        cli.printf("[error] There is no reduction strategy with name '%s'\n", strategy);
                    } else {
                        Debugger.strategy = option.get();
                        cli.printf("\nSwitched to reduction strategy %s (%s).\n", Debugger.strategy.getStrategyName(), Debugger.strategy.getStrategyIdentifier());
                        cli.printf("\n%s reduces to the %s of lambda terms.\n\n", LambdaCalculus.capitalize(Debugger.strategy.getStrategyName()), Debugger.strategy.getNormalFormName());
                    }

                } else {
                    cli.printf("\nThe current reduction strategy is %s (%s).\n", strategy.getStrategyName(), strategy.getStrategyIdentifier());
                    cli.printf("\n%s reduces to the %s of a lambda term.\n", LambdaCalculus.capitalize(strategy.getStrategyName()), strategy.getNormalFormName());
                    cli.printf("\nOther available reduction strategies are:\n");
                    cli.printf("%s\n\n", Arrays.stream(LambdaCalculus.strategies).filter(s -> !s.getStrategyIdentifier().equals(strategy.getStrategyIdentifier())).map(s -> "- " + s.getStrategyName() + " (" + s.getStrategyIdentifier() + ") which reduces to " + s.getNormalFormName()).collect(Collectors.joining("\n")));
                }
            }

        }.synonyms("s")
            .parameters(new CommandParameter("strategy identifier").optional().description("The identifier of a strategy.\n\nAvailable strategies are:\n" + Arrays.stream(LambdaCalculus.strategies).map(s -> "- " + s.getStrategyName() + " (" + s.getStrategyIdentifier() + ")").collect(Collectors.joining("\n"))))
            .description("Either returns the current reduction strategy (which is normal order by default) or sets it when one is provided."),

        new Command("help") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final Optional<Command> optCommand = Arrays.stream(commands).filter(c -> c.name.equals(arguments[0]) || c.synonyms().contains(arguments[0])).findFirst();

                    if (optCommand.isEmpty()) {
                        cli.printf("[error] Unknown debugger command with name '%s' (try 'help' for more information)\n", arguments[0]);
                        return;
                    }

                    final Command command = optCommand.get();

                    cli.printf("\nDebugger command: %s\n", command.name + command.parameters().stream().map(parameter -> "<" + parameter.name + (parameter.isOptional() ? "?" : "") + ">").collect(Collectors.joining(" ", " ", "")));
                    cli.printf("\n  Synonyms: %s\n", String.join(", ", command.synonyms()));
                    cli.printf("\n  Description:\n%s\n", Arrays.stream(command.description().split("\n")).map(line -> "    " + line).collect(Collectors.joining("\n")));

                    if (!command.parameters().isEmpty()) {
                        final AsciiTable table = new AsciiTable();

                        table.addRule();
                        table.addRow("Parameter", "Optional", "Description");

                        for (final CommandParameter parameter: command.parameters()) {
                            table.addRule();
                            final AT_Row row = table.addRow(parameter.name, parameter.isOptional() ? "Yes" : "No", parameter.description().replaceAll("\n", "<br>"));

                            row.getCells().get(0).getContext().setTextAlignment(TextAlignment.LEFT);
                            row.getCells().get(1).getContext().setTextAlignment(TextAlignment.LEFT);
                            row.getCells().get(2).getContext().setTextAlignment(TextAlignment.LEFT);
                        }

                        table.addRule();
                        table.setPadding(1);

                        CWC_LongestLine cwc = new CWC_LongestLine();
                        table.getRenderer().setCWC(cwc);
                        cwc.add(0, 40).add(0, 20).add(0, 60);

                        cli.printf("\n  Parameters:\n%s\n", Arrays.stream(table.render().split("\n")).map(line -> "    " + line).collect(Collectors.joining("\n")));
                    }

                    cli.printf("\n");
                } else {
                    final AsciiTable table = new AsciiTable();

                    table.addRule();
                    table.addRow("Command", "Synonyms", "Description");

                    for (final Command command : commands) {
                        table.addRule();
                        final AT_Row row = table.addRow(
                                command.name + command.parameters().stream().map(parameter -> "<" + parameter.name + (parameter.isOptional() ? "?" : "") + ">").collect(Collectors.joining(" ", " ", "")),
                                String.join(", ", command.synonyms()),
                                command.description().replaceAll("\n", "<br>")
                        );

                        row.getCells().get(0).getContext().setTextAlignment(TextAlignment.LEFT);
                        row.getCells().get(1).getContext().setTextAlignment(TextAlignment.LEFT);
                        row.getCells().get(2).getContext().setTextAlignment(TextAlignment.LEFT);
                    }

                    table.addRule();
                    table.setPadding(1);

                    CWC_LongestLine cwc = new CWC_LongestLine();
                    table.getRenderer().setCWC(cwc);
                    cwc.add(0, 40).add(0, 20).add(0, 60);

                    cli.printf("\nDebugger commands:\n%s\n\n", Arrays.stream(table.render().split("\n")).map(line -> "  " + line).collect(Collectors.joining("\n")));
                }
            }
        }.parameters(
            new CommandParameter("debugger command").description("The name of the debugger command for which a detailed description should be displayed.").optional()
        ).synonyms("h", "?").description("Either lists all available debugger commands or shows a detailed description if a name of a debugger command is provided."),

        new Command("exit") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                cli.printf("\nExiting debugger!\n\n");
                term = null;
                cli.setPrompt("\u03BB>");
            }

        }.synonyms("e", "q", "quit").description("Exits the interactive debugger and returns to the read-evaluate-print-loop."),
    };

    public static boolean isDebugging() {
        return term != null;
    }

}
