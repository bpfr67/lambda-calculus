package lc.syntax;

import lc.syntax.tree.Abstraction;
import lc.syntax.tree.Application;
import lc.syntax.tree.Term;
import lc.syntax.tree.Variable;
import org.petitparser.context.Result;
import org.petitparser.parser.combinators.ChoiceParser;
import org.petitparser.parser.combinators.SettableParser;
import org.petitparser.parser.primitive.CharacterParser;
import org.petitparser.parser.primitive.StringParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Parser {

    private Parser() {}

    private static final SettableParser abstraction = SettableParser.undefined();
    private static final SettableParser variable = SettableParser.undefined();
    private static final SettableParser list = SettableParser.undefined();
    private static final SettableParser string = SettableParser.undefined();
    private static final SettableParser atom = SettableParser.undefined();
    private static final SettableParser application = SettableParser.undefined();

    static {
        abstraction.set(
            CharacterParser.of('\\').or(CharacterParser.of('\u03BB')).trim()
                .seq(variable)
                .seq(CharacterParser.of('.').trim())
                .seq(abstraction)
                .<List<Term>, Term>map(terms -> new Abstraction(((Variable) terms.get(1)).name(), terms.get(3)))
            .or(application)
        );

        application.set(
            atom.trim().plus().<List<Term>, Term>map(terms -> terms.stream().skip(1).reduce(terms.get(0), Application::new))
        );

        atom.set(
            variable.trim()
            .or(CharacterParser.of('(').trim().seq(abstraction.trim()).seq(CharacterParser.of(')').trim()).<List<Term>, Term>map(values -> values.get(1)))
            .or(list)
            .or(string)
        );

        final ChoiceParser character = CharacterParser.noneOf("\"\\").or(CharacterParser.of('\\').seq(CharacterParser.any()).<List<Character>, Character>map(cs -> switch (cs.get(1)) {
            case 'n': yield '\n';
            case 't': yield '\t';
            case 'r': yield '\r';
            case 'f': yield '\f';
            case '0': yield '\0';
            default: yield cs.get(1);
        }));

        final org.petitparser.parser.Parser characters = character.star().<List<Character>, String>map(cs -> cs.stream().map(Object::toString).collect(Collectors.joining()));

        string.set(CharacterParser.of('"').seq(characters).seq(CharacterParser.of('"')).trim().<List<String>, Term>map(string -> {
            final String value = string.get(1);
            Term result = new Variable("nil");

            for (int i = value.length() - 1; i >= 0; --i) {
                int n = value.charAt(i);

                Term element = new Variable("x");

                while (n-- > 0) {
                    element = new Application(new Variable("f"), element);
                }

                element = new Abstraction("f", new Abstraction("x", element));

                result = new Application(new Application(new Variable("cons"), element), result);
            }

            return result;
        }));

        list.set(CharacterParser.of('[').trim().seq(abstraction.trim().separatedBy(CharacterParser.of(',').trim()).<List<Object>, List<Term>>map(objects -> {
            final ArrayList<Term> elements = new ArrayList<>();

            boolean skip = false;
            for (final Object object: objects) {
                if (skip) {
                    skip = false;
                } else {
                    skip = true;
                    elements.add((Term) object);
                }
            }

            return elements;
        }).optional(new ArrayList<>())).seq(CharacterParser.of(']').trim()).<List<List<Term>>, Term>map(values -> {
            Term result = new Variable("nil");
            List<Term> elements = values.get(1);

            Collections.reverse(elements);
            for (final Term element: elements) {
                result = new Application(new Application(new Variable("cons"), element), result);
            }

            return result;
        }));

        variable.set(CharacterParser.of('_').or(
                CharacterParser.range('a', 'z'),
                CharacterParser.range('A', 'Z'),
                CharacterParser.range('0', '9'),
                CharacterParser.anyOf("+-*~/!^$%&\\;:#<>=?`´|")
            ).plus()
            .<List<Character>, String>map(cs -> cs.stream().map(Objects::toString).collect(Collectors.joining()))
            .seq(CharacterParser.of('\'').star()
            .<List<Character>, String>map(cs -> cs.stream().map(Objects::toString).collect(Collectors.joining())))
            .<List<String>, Variable>map(strings -> new Variable(String.join("", strings)))
        );
    }

    public static Term parse(final String input) throws ParseException {
        final Result result = abstraction.trim().end().parse(input);

        if (result.isFailure()) {
            int startLine = input.lastIndexOf('\n', result.getPosition());
            int endLine = input.indexOf('\n', result.getPosition());

            if (startLine == -1) {
                startLine = 0;
            } else {
                startLine++;
            }

            if (endLine == -1) {
                endLine = input.length();
            }

            final String line = input.substring(startLine, endLine);
            final String before = input.substring(0, startLine);
            final String after = input.substring(endLine);
            final String indentation = line.substring(0, result.getPosition() - startLine).replaceAll("\\S", " ");
            final int row = before.isEmpty() ? 1 : before.split("\n").length + 1;
            final int col = result.getPosition() - startLine + 1;

            throw new ParseException(String.format("Syntax error in line %d at character %d:\n%s%s\n%s^ %s%s", row, col, before, line, indentation, result.getMessage(), after));
        } else {
            return result.get();
        }
    }

}
