package lc.syntax;

public final class ParseException extends Exception {

    public ParseException(final String message) {
        super(message);
    }

}
