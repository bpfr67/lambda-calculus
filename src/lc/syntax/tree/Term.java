package lc.syntax.tree;

import lc.common.Maybe;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public sealed interface Term permits Abstraction, Application, Lazy, Variable {

    Stream<String> free();

    Term substitute(final String name, final Term term);

    String show(final boolean numbers);

    Maybe<Integer> asNumber();

    Maybe<String> asString();

    Maybe<List<Term>> asList();

    static Term fromNumber(final int number) {
        Term term = new Variable("x");

        for (int i = 0; i < number; ++i) {
            term = new Application(new Variable("f"), term);
        }

        return new Abstraction("f", new Abstraction("x", term));
    }

    static Stream<String> fresh(final String source, final Stream<String> avoid) {
        final Matcher matcher = Pattern.compile("(\\D+)(\\d+)").matcher(source);

        final int start;
        final String prefix;
        if (matcher.matches()) {
            start = Integer.parseInt(matcher.group(2));
            prefix = matcher.group(1);
        } else {
            start = 0;
            prefix = source;
        }

        final Set<String> used = avoid.collect(Collectors.toSet());
        return IntStream.iterate(start, n -> n + 1).mapToObj(n -> prefix + n).filter(Predicate.not(used::contains));
    }

}
