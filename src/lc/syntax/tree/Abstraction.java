package lc.syntax.tree;

import lc.common.Maybe;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record Abstraction(String parameter, Term term) implements Term {

    public Term apply(final Term argument) {
        // From Wikipedia:
        // the beta-reduction of (\V.M) N is M[V := N].
        return this.term.substitute(this.parameter, argument);
    }

    @Override
    public Stream<String> free() {
        // a lambda abstraction binds its parameter name
        return this.term.free().filter(name -> !Objects.equals(name, this.parameter));
    }

    @Override
    public Term substitute(final String name, final Term term) {
        if (Objects.equals(this.parameter, name)) {
            // only substitute free names (`name` is bound here => stop substitution)
            return this;
        } else if (term.free().anyMatch(this.parameter::equals)) {
            // prevent accidental name capturing after substituting name with term (a free variable in term would be captured by this abstraction otherwise)
            final String fresh = Term.fresh(this.parameter, Stream.concat(this.term.free(), term.free())).findFirst().orElseThrow();
            return new Abstraction(fresh, this.term.substitute(this.parameter, new Variable(fresh)).substitute(name, term));
        } else {
            return new Abstraction(this.parameter, this.term.substitute(name, term));
        }
    }

    @Override
    public String toString() {
        return '\u03BB' + parameter + '.' + term;
    }

    @Override
    public String show(final boolean numbers) {
        final StringBuilder builder = new StringBuilder();

        this.asNumber().filter(n -> numbers).map(n -> (Object) n).or(this.asString()).or(this::asList).fold(
            object -> {
                if (object instanceof final Integer n) {
                    builder.append(n.intValue());
                } else if (object instanceof final String s) {
                    builder.append("\"");
                    for (int i = 0; i < s.length(); ++i) {
                        builder.append(switch (s.charAt(i)) {
                            case '\n': yield "\\n";
                            case '\t': yield "\\t";
                            case '\r': yield "\\r";
                            case '\b': yield "\\b";
                            case '\f': yield "\\f";
                            case '\0': yield "\\0";
                            case '"': yield "\\\"";
                            default: yield "" + s.charAt(i);
                        });
                    }
                    builder.append("\"");
                } else if (object instanceof List) {
                    @SuppressWarnings("unchecked")
                    final List<Term> xs = (List<Term>) object;
                    builder.append(xs.stream().map(x -> x.show(numbers)).collect(Collectors.joining(", ", "[", "]")));
                }
                return null;
            },
            () -> {
                builder.append('\u03BB');
                builder.append(parameter);
                builder.append('.');
                builder.append(term.show(numbers));
                return null;
            }
        );

        return builder.toString();
    }

    @Override
    public Maybe<String> asString() {
        return asList().flatMap(xs -> {
            final List<Maybe<Integer>> numbers = xs.stream().map(Term::asNumber).toList();
            if (numbers.stream().allMatch(Maybe::nonEmpty)) {
                final int[] characters = numbers.stream().map(Maybe::get).mapToInt(n -> n).toArray();
                if (Arrays.stream(characters).allMatch(n -> n >= 0 && n < 128)) {
                    return Maybe.of(Arrays.stream(characters).mapToObj(n -> "" + (char) n).collect(Collectors.joining()));
                }
            }

            return Maybe.nothing();
        });
    }

    @Override
    public Maybe<List<Term>> asList() {
        if (this.term instanceof final Application cons1) {
            if (cons1.left() instanceof final Application cons0) {
                if (!(cons0.left() instanceof Variable variable) || !variable.name().equals(this.parameter)) {
                    return Maybe.nothing();
                }

                return cons1.right().asList().map(xs -> {
                    xs.add(0, cons0.right());
                    return xs;
                });
            }
        } else if (this.term instanceof final Abstraction x) {
            if (x.term instanceof Variable variable && variable.name().equals(x.parameter)) {
                return Maybe.of(new LinkedList<>());
            }
        }

        return Maybe.nothing();
    }

    @Override
    public Maybe<Integer> asNumber() {
        if (this.term instanceof final Abstraction x) {
            final Variable fName = new Variable(this.parameter);
            final Variable xName = new Variable(x.parameter());

            Term encoding = x.term();
            int n = 0;
            while (encoding instanceof final Application application) {
                if (application.left().equals(fName)) {
                    encoding = application.right();
                    n += 1;
                } else {
                    break;
                }
            }

            if (encoding.equals(xName)) {
                return Maybe.of(n);
            }
        }

        return Maybe.nothing();
    }

}
