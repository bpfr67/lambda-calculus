package lc.syntax.tree;

import lc.common.Maybe;

import java.util.List;
import java.util.stream.Stream;

public record Application(Term left, Term right) implements Term {

    @Override
    public Stream<String> free() {
        return Stream.concat(this.left.free(), this.right.free());
    }

    @Override
    public Term substitute(final String name, final Term term) {
        return new Application(
            this.left.substitute(name, term),
            this.right.substitute(name, term)
        );
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        if (left instanceof Abstraction) {
            builder.append('(');
            builder.append(left);
            builder.append(')');
        } else {
            builder.append(left);
        }

        builder.append(' ');

        if (right instanceof Application || right instanceof Abstraction) {
            builder.append('(');
            builder.append(right);
            builder.append(')');
        } else {
            builder.append(right);
        }

        return builder.toString();
    }

    @Override
    public String show(final boolean numbers) {
        final StringBuilder builder = new StringBuilder();

        if (left instanceof Abstraction) {
            builder.append('(');
            builder.append(left.show(numbers));
            builder.append(')');
        } else {
            builder.append(left.show(numbers));
        }

        builder.append(' ');

        if (right instanceof Application || (right instanceof Abstraction && right.asNumber().isEmpty())) {
            builder.append('(');
            builder.append(right.show(numbers));
            builder.append(')');
        } else {
            builder.append(right.show(numbers));
        }

        return builder.toString();
    }

    @Override
    public Maybe<Integer> asNumber() {
        return Maybe.nothing();
    }

    @Override
    public Maybe<List<Term>> asList() {
        return Maybe.nothing();
    }

    @Override
    public Maybe<String> asString() {
        return Maybe.nothing();
    }

}
