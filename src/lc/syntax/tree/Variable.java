package lc.syntax.tree;

import lc.common.Maybe;
import java.util.List;
import java.util.Objects;import java.util.stream.Stream;

public record Variable(String name) implements Term {

    @Override
    public Stream<String> free() {
        return Stream.of(this.name);
    }

    @Override
    public Term substitute(final String name, final Term term) {
        if (Objects.equals(this.name, name)) {
            // replace this variable by the provided term
            return term;
        } else {
            return this;
        }
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public String show(final boolean numbers) {
        return Objects.toString(this.name);
    }

    @Override
    public Maybe<Integer> asNumber() {
        return Maybe.nothing();
    }

    @Override
    public Maybe<List<Term>> asList() {
        return Maybe.nothing();
    }

    @Override
    public Maybe<String> asString() {
        return Maybe.nothing();
    }

}
