package lc.syntax.tree;

import lc.common.Maybe;

import java.util.List;
import java.util.stream.Stream;

public final class Lazy implements Term {

    public Term term;

    public Lazy(final Term term) {
        this.term = term;
    }

    @Override
    public Stream<String> free() {
        return term.free();
    }

    @Override
    public Term substitute(final String name, final Term term) {
        return this.term.substitute(name, term);
    }

    @Override
    public String toString() {
        return term.toString();
    }

    @Override
    public String show(final boolean numbers) {
        return term.show(numbers);
    }

    @Override
    public Maybe<Integer> asNumber() {
        return term.asNumber();
    }

    @Override
    public Maybe<List<Term>> asList() {
        return term.asList();
    }

    @Override
    public Maybe<String> asString() {
        return term.asString();
    }

}
