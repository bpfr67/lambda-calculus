package lc.reduction;

import lc.syntax.tree.Abstraction;
import lc.syntax.tree.Application;
import lc.syntax.tree.Lazy;
import lc.syntax.tree.Term;

/**
 * A strategy that always reduces the leftmost outermost redex first.
 * That is, this strategy also performs reduction under lambda abs-
 * traction. The result is a lambda term in normal form.
 */
public record NormalOrder() implements ReductionStrategy {

    private static final ReductionStrategy callByName = new CallByName();

    @Override
    public String getStrategyName() {
        return "normal order";
    }

    @Override
    public String getStrategyIdentifier() {
        return "no";
    }

    @Override
    public String getNormalFormName() {
        return "normal form";
    }

    @Override
    public boolean isNormal(final Term term) {
        if (term instanceof final Application application) {

            if (application.left() instanceof Abstraction) {
                return false;
            } else {
                return isNormal(application.left()) && isNormal(application.right());
            }
        } else if (term instanceof final Abstraction abstraction) {
            return isNormal(abstraction.term());
        } else if (term instanceof final Lazy lazy) {
            return isNormal(lazy.term);
        } else {
            return true;
        }
    }

    @Override
    public Term reduce(final Term term) {
        if (term instanceof final Application application) {

            // important: reduce the left term with the call-by-name strategy
            // otherwise reduction is performed under lambda abstractions before
            // any function call and this would not be the outermost redex.
            if (callByName.isNormal(application.left())) {
                if (application.left() instanceof final Abstraction abstraction) {
                    return abstraction.apply(application.right());
                } else if (!isNormal(application.left())) {
                    return new Application(reduce(application.left()), application.right());
                } else {
                    return new Application(application.left(), reduce(application.right()));
                }
            } else {
                return new Application(callByName.reduce(application.left()), application.right());
            }
        } else if (term instanceof final Abstraction abstraction) {
            return new Abstraction(abstraction.parameter(), reduce(abstraction.term()));
        } else if (term instanceof final Lazy lazy) {
            // handle lazy values as if they were not there
            return reduce(lazy.term);
        } else {
            return term;
        }
    }

}
