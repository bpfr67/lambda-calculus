package lc.reduction;

import lc.syntax.tree.Abstraction;
import lc.syntax.tree.Application;
import lc.syntax.tree.Lazy;
import lc.syntax.tree.Term;

/**
 * A strategy that always reduces the leftmost innermost redex first. The result is
 * a lambda term in normal form. This strategy differs from the call-by-value
 * strategy only by reducing also under lambda abstractions.
 */
public record ApplicativeOrder() implements ReductionStrategy {

    @Override
    public String getStrategyName() {
        return "applicative order";
    }

    @Override
    public String getStrategyIdentifier() {
        return "ao";
    }

    @Override
    public String getNormalFormName() {
        return "normal form";
    }

    @Override
    public boolean isNormal(final Term term) {
        if (term instanceof final Application application) {
            return !(application.left() instanceof Abstraction)
                && isNormal(application.left())
                && isNormal(application.right());
        } else if (term instanceof final Abstraction abstraction) {
            return isNormal(abstraction.term());
        } else if (term instanceof final Lazy lazy) {
            return isNormal(lazy.term);
        } else {
            return true;
        }
    }

    @Override
    public Term reduce(final Term term) {
        if (term instanceof final Application application) {
            if (!isNormal(application.left())) {
                return new Application(reduce(application.left()), application.right());
            } else if (!isNormal(application.right())) {
                return new Application(application.left(), reduce(application.right()));
            } else if (application.left() instanceof final Abstraction abstraction) {
                // perform beta-reduction
                return abstraction.apply(reduce(application.right()));
            } else {
                return application;
            }
        } else if (term instanceof final Abstraction abstraction) {
            return new Abstraction(abstraction.parameter(), reduce(abstraction.term()));
        } else if (term instanceof final Lazy lazy) {
            // handle lazy values as if they were not there
            return reduce(lazy.term);
        } else {
            return term;
        }
    }

}
