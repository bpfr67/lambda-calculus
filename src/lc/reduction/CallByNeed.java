package lc.reduction;

import lc.syntax.tree.Abstraction;
import lc.syntax.tree.Application;
import lc.syntax.tree.Lazy;
import lc.syntax.tree.Term;

public record CallByNeed() implements ReductionStrategy {

    @Override
    public String getStrategyName() {
        return "call-by-need";
    }

    @Override
    public String getStrategyIdentifier() {
        return "le";
    }

    @Override
    public String getNormalFormName() {
        return "weak head normal form";
    }

    @Override
    public boolean isNormal(final Term term) {
        if (term instanceof final Application application) {
            return !(application.left() instanceof Abstraction) && isNormal(application.left());
        } else if (term instanceof final Lazy lazy) {
            return isNormal(lazy.term);
        } else {
            return true;
        }
    }

    @Override
    public Term reduce(final Term term) {
        if (term instanceof final Application application) {
            if (application.left() instanceof final Abstraction abstraction) {
                // perform beta-reduction
                return abstraction.apply(new Lazy(application.right()));
            } else {
                return new Application(reduce(application.left()), application.right());
            }
        } else if (term instanceof final Lazy lazy) {
            // handle lazy values as if they were not there
            lazy.term = reduce(lazy.term);

            if (isNormal(lazy.term)) {
                return lazy.term;
            } else {
                return lazy;
            }
        } else {
            return term;
        }
    }

}
