package lc.reduction;

import lc.syntax.tree.Abstraction;
import lc.syntax.tree.Application;
import lc.syntax.tree.Lazy;
import lc.syntax.tree.Term;

/**
 * A strategy that does not perform reduction under lambda abstractions
 * and always reduces the leftmost outermost redex first. The result is
 * a lambda term in weak head normal form.
 */
public record CallByName() implements ReductionStrategy {

    @Override
    public String getStrategyName() {
        return "call-by-name";
    }

    @Override
    public String getStrategyIdentifier() {
        return "bn";
    }

    @Override
    public String getNormalFormName() {
        return "weak head normal form";
    }

    @Override
    public boolean isNormal(final Term term) {
        if (term instanceof final Application application) {
            return !(application.left() instanceof Abstraction) && isNormal(application.left());
        } else if (term instanceof final Lazy lazy) {
            return isNormal(lazy.term);
        } else {
            return true;
        }
    }

    @Override
    public Term reduce(final Term term) {
        if (term instanceof final Application application) {
            if (application.left() instanceof final Abstraction abstraction) {
                // perform beta-reduction
                return abstraction.apply(application.right());
            } else {
                return new Application(reduce(application.left()), application.right());
            }
        } else if (term instanceof final Lazy lazy) {
            // handle lazy values as if they were not there
            return reduce(lazy.term);
        } else {
            return term;
        }
    }

}
