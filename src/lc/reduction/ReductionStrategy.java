package lc.reduction;

import lc.syntax.tree.Term;

public interface ReductionStrategy {

    String getStrategyName();

    String getStrategyIdentifier();

    String getNormalFormName();

    boolean isNormal(final Term term);

    Term reduce(final Term term);

}
