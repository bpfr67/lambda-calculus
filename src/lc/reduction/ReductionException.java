package lc.reduction;

public final class ReductionException extends RuntimeException {

    public ReductionException(final String message) {
        super(message);
    }

}
