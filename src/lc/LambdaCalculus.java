package lc;

import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import lc.cli.Command;
import lc.cli.CommandException;
import lc.cli.CommandLineInterface;
import lc.cli.CommandParameter;
import lc.common.Context;
import lc.common.Maybe;
import lc.reduction.*;
import lc.syntax.ParseException;
import lc.syntax.Parser;
import lc.syntax.tree.Term;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class LambdaCalculus {

    public static final String version = "2.5.0-alpha";

    public static final List<Context> contexts = new ArrayList<>();

    public static Context context = new Context("prelude");

    public static final ReductionStrategy[] strategies = new ReductionStrategy[] {
        new NormalOrder(),
        new ApplicativeOrder(),
        new CallByName(),
        new CallByValue(),
        new CallByNeed()
    };

    public static ReductionStrategy strategy = strategies[0];

    public static boolean global = true;

    public static boolean naturals = false;

    public static CommandLineInterface cli;

    private static final Command[] commands = new Command[] {
        new Command("debug") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final Term term;
                try {
                    term = Parser.parse(arguments[0]);
                    cli.printf("\nStarting debugger with term:\n\n%s\n\nType 'help' for more information.\n\nThe reduction strategy is %s (%s).\n\n", term.show(naturals), Debugger.strategy.getStrategyName(), Debugger.strategy.getStrategyIdentifier());
                    Debugger.term = term;
                    cli.setPrompt("[debug] \u03BB:");
                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception.getMessage());
                }
            }

        }.synonyms("d").description("Allows interactive debugging of the provided term.").parameters(new CommandParameter("term").description("The term that should be debugged.").term()),

        new Command("naturals") {
            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final String enable = arguments[0].trim().toLowerCase();
                    if (enable.equals("y") || enable.equals("yes") || enable.equals("true") || enable.equals("t") || enable.equals("1")) {
                        LambdaCalculus.naturals = true;
                    } else if (enable.equals("n") || enable.equals("no") || enable.equals("false") || enable.equals("f") || enable.equals("0")) {
                        LambdaCalculus.naturals = false;
                    } else {
                        cli.printf("[error] Illegal boolean value '%s'\n", enable);
                        return;
                    }
                } else {
                    LambdaCalculus.naturals = !LambdaCalculus.naturals;
                }

                Debugger.naturals = LambdaCalculus.naturals;
                if (LambdaCalculus.naturals) {
                    cli.printf("\nEnabled the automatic conversion between terms and natural numbers.\n\n");
                } else {
                    cli.printf("\nDisabled the automatic conversion between terms and natural numbers.\n\n");
                }
            }
        }.synonyms("nat").description("Enables or disables the automatic conversion between terms and natural numbers.")
            .parameters(new CommandParameter("enable").optional().description("Whether or not the natural number mode should be enabled.")),

        new Command("free") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                try {
                    final Term term = bind(Parser.parse(arguments[0]));
                    final Set<String> fv = term.free().collect(Collectors.toSet());

                    cli.printf("\nThe term\n\n%s\n\ncontains %d free variable%s%s\n\n", term.show(naturals), fv.size(), fv.size() == 1 ? "" : "s", fv.isEmpty() ? "." : ":\n\n" + String.join(", ", fv));

                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception.getMessage());
                }
            }

        }.synonyms("f").description("Searches the provided term for all occurrences of free variables and lists them.").parameters(new CommandParameter("term").description("The term whose free variables should be listed.").term()),

        new Command("global") {
            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final String enable = arguments[0].trim().toLowerCase();
                    if (enable.equals("y") || enable.equals("yes") || enable.equals("true") || enable.equals("t") || enable.equals("1")) {
                        LambdaCalculus.global = true;
                    } else if (enable.equals("n") || enable.equals("no") || enable.equals("false") || enable.equals("f") || enable.equals("0")) {
                        LambdaCalculus.global = false;
                    } else {
                        cli.printf("[error] Illegal boolean value '%s'\n", enable);
                        return;
                    }
                } else {
                    LambdaCalculus.global = !LambdaCalculus.global;
                }

                cli.printf("\nSwitched to %s mode.\n\nLambda terms will %s.\n\n", LambdaCalculus.global ? "global" : "local", LambdaCalculus.global ? "be substituted using global bindings" : "not be substituted anymore");
            }

        }.synonyms("g").parameters(new CommandParameter("enable").optional().description("Whether or not global mode should be enabled."))
            .description("Enables or disables whether terms should be automatically bound by the global context of the read-evaluate-print-loop. If disabled, global bindings like 'id' are not substituted anymore."),

        new Command("normal") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                try {
                    final Term term = bind(Parser.parse(arguments[0]));
                    cli.printf("\nThe term\n\n%s\n\nis%s in %s\n\n", Arrays.stream(term.show(naturals).split("\n")).map(line -> "  " + line).collect(Collectors.joining("\n")), strategy.isNormal(term) ? "" : " not", strategy.getNormalFormName());
                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception.getMessage());
                }
            }

        }.synonyms("n").description("Checks if the provided term is in normal form that corresponds to the current reduction strategy.").parameters(new CommandParameter("term").term().description("The term which should be checked for its form.")),
        new Command("strategy") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final String strategy = arguments[0];
                    final Optional<ReductionStrategy> option = Arrays.stream(strategies).filter(s -> s.getStrategyName().equalsIgnoreCase(strategy) || s.getStrategyIdentifier().equalsIgnoreCase(strategy)).findFirst();

                    if (option.isEmpty()) {
                        cli.printf("[error] There is no reduction strategy with name '%s'\n", strategy);
                    } else {
                        LambdaCalculus.strategy = Debugger.strategy = option.get();
                        cli.printf("\nSwitched to reduction strategy %s (%s).\n", LambdaCalculus.strategy.getStrategyName(), LambdaCalculus.strategy.getStrategyIdentifier());
                        cli.printf("\n%s reduces to the %s of lambda terms.\n\n", capitalize(LambdaCalculus.strategy.getStrategyName()), LambdaCalculus.strategy.getNormalFormName());
                    }

                } else {
                    cli.printf("\nThe current reduction strategy is %s (%s).\n", strategy.getStrategyName(), strategy.getStrategyIdentifier());
                    cli.printf("\n%s reduces to the %s of a lambda term.\n", capitalize(strategy.getStrategyName()), strategy.getNormalFormName());
                    cli.printf("\nOther available reduction strategies are:\n");
                    cli.printf("%s\n\n", Arrays.stream(strategies).filter(s -> !s.getStrategyIdentifier().equals(strategy.getStrategyIdentifier())).map(s -> "- " + s.getStrategyName() + " (" + s.getStrategyIdentifier() + ") which reduces to " + s.getNormalFormName()).collect(Collectors.joining("\n")));
                }
            }

        }.synonyms("s")
            .parameters(new CommandParameter("strategy identifier").optional().description("The identifier of a strategy.\n\nAvailable strategies are:\n" + Arrays.stream(strategies).map(s -> "- " + s.getStrategyName() + " (" + s.getStrategyIdentifier() + ")").collect(Collectors.joining("\n"))))
            .description("Either returns the current reduction strategy (which is normal order by default) or sets it when one is provided."),

        new Command("help") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                if (arguments.length > 0) {
                    final Optional<Command> optCommand = Arrays.stream(commands).filter(c -> c.name.equals(arguments[0]) || c.synonyms().contains(arguments[0])).findFirst();

                    if (optCommand.isEmpty()) {
                        cli.printf("[error] Unknown command with name '%s' (try ':help' for more information)\n", arguments[0]);
                        return;
                    }

                    final Command command = optCommand.get();

                    cli.printf("\nCommand: %s\n", command.name + command.parameters().stream().map(parameter -> "<" + parameter.name + (parameter.isOptional() ? "?" : "") + ">").collect(Collectors.joining(" ", " ", "")));
                    cli.printf("\n  Synonyms: %s\n", String.join(", ", command.synonyms()));
                    cli.printf("\n  Description:\n%s\n", Arrays.stream(command.description().split("\n")).map(line -> "    " + line).collect(Collectors.joining("\n")));

                    if (!command.parameters().isEmpty()) {
                        final AsciiTable table = new AsciiTable();

                        table.addRule();
                        table.addRow("Parameter", "Optional", "Description");

                        for (final CommandParameter parameter: command.parameters()) {
                            table.addRule();
                            final AT_Row row = table.addRow(parameter.name, parameter.isOptional() ? "Yes" : "No", parameter.description().replaceAll("\n", "<br>"));

                            row.getCells().get(0).getContext().setTextAlignment(TextAlignment.LEFT);
                            row.getCells().get(1).getContext().setTextAlignment(TextAlignment.LEFT);
                            row.getCells().get(2).getContext().setTextAlignment(TextAlignment.LEFT);
                        }

                        table.addRule();
                        table.setPadding(1);

                        CWC_LongestLine cwc = new CWC_LongestLine();
                        table.getRenderer().setCWC(cwc);
                        cwc.add(0, 40).add(0, 20).add(0, 60);

                        cli.printf("\n  Parameters:\n%s\n", Arrays.stream(table.render().split("\n")).map(line -> "    " + line).collect(Collectors.joining("\n")));
                    }

                    cli.printf("\n");
                } else {
                    final AsciiTable table = new AsciiTable();

                    table.addRule();
                    table.addRow("Command", "Synonyms", "Description");

                    for (final Command command : commands) {
                        table.addRule();
                        final AT_Row row = table.addRow(
                            ":" + command.name + command.parameters().stream().map(parameter -> "<" + parameter.name + (parameter.isOptional() ? "?" : "") + ">").collect(Collectors.joining(" ", " ", "")),
                            String.join(", ", command.synonyms()),
                            command.description().replaceAll("\n", "<br>")
                        );

                        row.getCells().get(0).getContext().setTextAlignment(TextAlignment.LEFT);
                        row.getCells().get(1).getContext().setTextAlignment(TextAlignment.LEFT);
                        row.getCells().get(2).getContext().setTextAlignment(TextAlignment.LEFT);
                    }

                    table.addRule();
                    table.setPadding(1);

                    CWC_LongestLine cwc = new CWC_LongestLine();
                    table.getRenderer().setCWC(cwc);
                    cwc.add(0, 40).add(0, 20).add(0, 60);

                    cli.printf("\nCommands:\n%s\n\n", Arrays.stream(table.render().split("\n")).map(line -> "  " + line).collect(Collectors.joining("\n")));
                }
            }
        }.parameters(
            new CommandParameter("command").description("The name of the command for which a detailed description should be displayed.").optional()
        ).synonyms("h", "?").description("Either lists all available commands or shows a detailed description if a name of a command is provided."),

        new Command("describe") {
            @Override
            public void execute(String[] arguments) throws IOException {
                final String name = arguments[0];
                final Maybe<String> description = Maybe.ofNull(arguments[1]).map(String::trim).filter(Predicate.not(String::isEmpty));

                Context context = LambdaCalculus.context;
                Maybe<Context.Entry> entry = context.get(name);
                while (entry.isEmpty() && !context.parent.isEmpty()) {
                    context = context.parent.get();
                    entry = context.get(name);
                }

                if (entry.isEmpty()) {
                    cli.printf("[error] Unknown binding '%s' in context '%s'\n", name, LambdaCalculus.context.name);
                    return;
                }

                context.insert(name, entry.get().term(), description);
            }
        }.synonyms("dsc").description("Adds a description to an existing binding.").parameters(
            new CommandParameter("name").description("The name of the binding."),
            new CommandParameter("description").description("The description for the binding.")
        ),

        new Command("bind") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final String name = arguments[0];
                final String term = arguments[1];

                try {
                    final Term result = Parser.parse(term);

                    if (result.free().anyMatch(name::equals)) {
                        cli.printf("[error] Recursive bindings are not allowed (referring to binding '%s')\n", name);
                        return;
                    }

                    context.insert(name, result, Maybe.nothing());
                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception);
                }
            }

        }.synonyms("b").parameters(
            new CommandParameter("name").description("The name which should be bound to the provided term."),
            new CommandParameter("term").description("A term of the lambda calculus.").term()
        ).description("Binds the provided name to a term of the lambda calculus. Any occurrence of this name in future terms will be substituted with the term. An optional description may be provided for documentation purposes."),

        new Command("unbind") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final String name = arguments[0];

                if (context.contains(name)) {
                    context.remove(name);
                } else {
                    cli.printf("[error] There is no binding with name '%s' in the current context '%s'\n", name, context.name);
                }
            }

        }.synonyms("u").parameters(
            new CommandParameter("name").description("The name which should be unbound.")
        ).description("Unbinds the provided name such that it is handled as a free variable in future terms."),

        new Command("list") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                final Pattern pattern = Pattern.compile(arguments.length > 0 ? arguments[0] : "(.|\n)*");


                Maybe<Context> current = Maybe.of(context);
                while (current.nonEmpty()) {
                    final Context context = current.get();

                    final AsciiTable table = new AsciiTable();

                    table.addRule();
                    table.addRow("Name", "Description");

                    int count = 0;
                    for (final Context.Entry entry: context) {
                        if (!pattern.matcher(entry.name()).matches()) {
                            continue;
                        }

                        count++;
                        table.addRule();
                        final AT_Row row = table.addRow(
                                entry.name(),
                                entry.documentation().getOrElse(() -> "").replaceAll("\n", "<br>")
                        );

                        row.getCells().get(0).getContext().setTextAlignment(TextAlignment.LEFT);
                        row.getCells().get(1).getContext().setTextAlignment(TextAlignment.LEFT);
                    }

                    table.addRule();
                    table.setPadding(1);

                    CWC_LongestLine cwc = new CWC_LongestLine();
                    table.getRenderer().setCWC(cwc);
                    cwc.add(0, 40).add(30, 80);

                    if (count > 0) {
                        cli.printf("\nThere %s %d binding%s available%s in the context '%s':\n%s\n\n", count == 1 ? "is" : "are", count, count == 1 ? "" : "s", arguments.length > 0 ? " matching the regular expression '" + arguments[0] + "'" : "", context.name, Arrays.stream(table.render().split("\n")).map(line -> "  " + line).collect(Collectors.joining("\n")));
                    } else {
                        cli.printf("\nThere are no bindings available%s in the context '%s'!\n\n", arguments.length > 0 ? " matching the regular expression '" + arguments[0] + "'" : "", context.name);
                    }

                    current = context.parent;
                }
            }
        }.synonyms("l").parameters(
            new CommandParameter("filter").optional().description("An optional regular expression which is used to show only matching bindings.")
        ).description("Lists all available bindings."),

        new Command("inspect") {

            @Override
            public void execute(String[] arguments) throws IOException {

                try {
                    final Term term = bind(Parser.parse(arguments[0]));

                    cli.printf("\nTerm:\n  %s\n", term.show(naturals));

                    if (context.contains(arguments[0])) {
                        final Context.Entry entry = context.get(arguments[0]).get();

                        cli.printf("\nCorresponding binding:\n  %s = %s\n", entry.name(), entry.term().show(naturals));
                        if (entry.documentation().nonEmpty()) {
                            cli.printf("\nDescription:\n%s\n", Arrays.stream(entry.documentation().get().split("\n")).map(line -> "  " + line).collect(Collectors.joining("\n")));
                        }
                    }

                    final Set<String> fv = term.free().collect(Collectors.toSet());
                    cli.printf("\nFree variables (%d): %s\n", fv.size(), fv.isEmpty() ? "none" : String.join(", ", fv));

                    try {
                        cli.printf("\nNormal form:\n  %s\n", normalize(new NormalOrder(), 5000).apply(term).show(naturals));
                    } catch (final ReductionException exception) {
                        cli.printf("\nNormal form:\n  %s\n", exception.getMessage());
                    }

                    try {
                        cli.printf("\nWeak normal form:\n  %s\n", normalize(new CallByValue(), 5000).apply(term).show(naturals));
                    } catch (final ReductionException exception) {
                        cli.printf("\nWeak normal form:\n  %s\n", exception.getMessage());
                    }

                    try {
                        cli.printf("\nWeak head normal form:\n  %s\n\n", normalize(new CallByName(), 5000).apply(term).show(naturals));
                    } catch (final ReductionException exception) {
                        cli.printf("\nWeak head normal form:\n  %s\n", exception.getMessage());
                    }
                } catch (final ParseException exception) {
                    cli.printf("[error] %s\n", exception.getMessage());
                }
            }

        }.synonyms("i").parameters(
            new CommandParameter("term").description("The term or binding which should be inspected.").term()
        ),

        new Command("exit") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                cli.printf("Bye Bye!\n");
                System.exit(0);
            }

        }.synonyms("e", "q", "quit").description("Exits the interactive read-evaluate-print-loop and terminates the process."),

        new Command("version") {

            @Override
            public void execute(final String[] arguments) throws IOException {
                cli.printf("\nYou are using version %s.\n\nVisit https://git.thm.de/bpfr67/lambda-calculus to check\nif you have the most recent version.\n\n", version);
            }

        }.synonyms("v").description("Shows the version of the lambda-calculus you are currently running.")
    };

    public static String capitalize(final String string) {
        if (string == null || string.isEmpty()) {
            return string;
        }

        return Character.toUpperCase(string.charAt(0)) + string.substring(1);
    }

    private LambdaCalculus() { }

    public static Term bind(Term term) {
        if (global) {
            List<String> free = term.free().filter(context::exists).toList();
            while (!free.isEmpty()) {
                for (final String name: free) {
                    term = term.substitute(name, context.lookup(name).get().term());
                }
                free = term.free().filter(context::exists).toList();
            }
        }

        if (naturals) {
            final List<String> numbers = term.free().filter(variable -> variable.matches("\\d+")).toList();

            do {
                for (final String number: numbers) {
                    term = term.substitute(number, Term.fromNumber(Integer.parseInt(number)));
                }
            } while (term.free().anyMatch(numbers::contains));
        }

        return term;
    }

    public static Function<Term, Term> normalize(final ReductionStrategy strategy, final int depth) {
        return term -> {
            int i = 0;

            while ((depth < 0 || i++ < depth) && !strategy.isNormal(term)) {
                term = strategy.reduce(term);
            }

            if (strategy.isNormal(term)) {
                return term;
            } else {
                throw new ReductionException("no normal form reached after " + (i - 1) + " iterations");
            }
        };
    }

    private static void introduce(final String name, final String term, final String documentation) {
        try {
            final Term parsed = Parser.parse(term);

            if (parsed.free().anyMatch(name::equals)) {
                try {
                    cli.printf("[error] Recursive bindings are not allowed (referring to binding '%s')\n", name);
                } catch (final IOException exception) {
                    throw new RuntimeException(exception);
                }
            }

            context.insert(name, parsed, Maybe.ofNull(documentation));
        } catch (final ParseException exception) {
            try {
                cli.printf("[warning] Failed to bind '%s': %s\n", name, exception.getMessage());
            } catch (final IOException other) {
                throw new RuntimeException(other);
            }
        }
    }

    static {
        contexts.add(context);
    }

    public static void main(final String[] arguments) throws IOException {
        if (Arrays.asList(arguments).contains("--prelude")) {
            // common stuff
            introduce("id", "\u03BBx.x", "The identity function. It always returns the supplied argument.");
            introduce("flip", "\u03BBf.\u03BBx.\u03BBy.f y x", "");

            // arithmetic
            introduce("zero", "false", "");
            introduce("one", "\u03BBf.\u03BBx.f x", "");
            introduce("add", "\u03BBm.\u03BBn.\u03BBf.\u03BBx.m f (n f x)", "The arithmetic addition for peano encoded numbers.");
            introduce("mul", "\u03BBm.\u03BBn.\u03BBf.n (m f)", "The arithmetic multiplication for peano encoded numbers.");
            introduce("pow", "\u03BBm.\u03BBn.n m", "");
            introduce("sub", "\u03BBm.\u03BBn.n dec m", "The arithmetic subtraction for peano encoded numbers. It always returns zero if the argument is zero.");
            introduce("inc", "\u03BBn.add one n", "The increment function. It always increments its peano encoded number by one.");
            introduce("dec", "\u03BBn.\u03BBf.\u03BBx.n (\u03BBg.\u03BBh.h (g f)) (\u03BBu.x) (\u03BBu.u)", "The decrement function. It always decrements its peano encoded number by one.");
            introduce("is-zero", "\u03BBn.n (\u03BBt.false) true", "A predicate to test whether the argument is the peano encoded number zero.");

            // fixed point combinators
            introduce("Y", "\u03BBf.(\u03BBx.f (x x)) (\u03BBx.f (x x))", "The fixed-point combinator Y (lazy). It computes the fixed-point for the supplied generating function.");
            introduce("Z", "\u03BBh.(\u03BBx.\u03BBa.h (x x) a) (\u03BBx.\u03BBa.h (x x) a)", "The fixed-point combinator Z (strict). It computes the fixed-point for the supplied generating function.");

            // booleans
            introduce("true", "\u03BBx.\u03BBy.x", "The boolean constant representing true.");
            introduce("false", "\u03BBx.\u03BBy.y", "The boolean constant representing false.");
            introduce("if", "\u03BBc.\u03BBa.\u03BBb.c a b", "A more readable version of a conditional. If the first argument is true, the second argument is returned. Otherwise the third argument is returned.");
            introduce("neg", "flip", "");

            // pairs
            introduce("pair", "\u03BBx.\u03BBy.\u03BBz.z x y", "");
            introduce("fst", "\u03BBp.p true", "");
            introduce("snd", "\u03BBp.p false", "");

            // lists
            introduce("nil", "false", "");
            introduce("cons", "pair", "");
            introduce("is-nil", "\u03BBxs.xs (\u03BBx.\u03BBxs.\u03BBb.false) true", "");
            introduce("fold", "Y (\u03BBfold.\u03BBf.\u03BBz.\u03BBxs.if (is-nil xs) z (xs (\u03BBx.\u03BBxs.f x (fold f z xs))))", "");
            introduce("length", "\u03BBxs.fold (\u03BBcurrent.\u03BBprevious.inc previous) zero xs", "");

            // the factorial function
            introduce("factorial", "Y (\u03BBfactorial.\u03BBn.if (is-zero n) one (mul (factorial (dec n)) n))", "The factorial function on basis of the Y combinator.");
        }

        context = new Context(Maybe.of(context), "user");
        contexts.add(context);
        context.load();

        cli = new CommandLineInterface("\u03BB>") {

            @Override
            public void accept(final String input) throws IOException {
                if (Debugger.isDebugging()) {
                    final String line = input.trim();

                    for (final Command command : Debugger.commands) {
                        try {
                            if (command.parse(line)) {
                                return;
                            }
                        } catch (final CommandException exception) {
                            cli.printf("[error] %s\n", exception.getMessage());
                            return;
                        }
                    }

                    cli.printf("[error] Unknown debugger command with name '%s' (try 'help' for more information)\n", line.split("\\s+")[0]);
                } else if (input.trim().startsWith(":")) {
                    final String line = input.trim().substring(1).trim();

                    for (final Command command : commands) {
                        try {
                            if (command.parse(line)) {
                                return;
                            }
                        } catch (final CommandException exception) {
                            cli.printf("[error] %s\n", exception.getMessage());
                            return;
                        } catch (final IOException exception) {
                            throw exception;
                        } catch (final Throwable throwable) {
                            cli.printf("[error] %s\n", throwable.getMessage());
                            throwable.printStackTrace();
                            return;
                        }
                    }

                    cli.printf("[error] Unknown command with name '%s' (try ':help' for more information)\n", line.split("\\s+")[0]);
                } else {
                    try {
                        Term term = bind(Parser.parse(input));

                        while (!strategy.isNormal(term)) {
                            term = strategy.reduce(term);
                        }

                        cli.printf("%s\n", term.show(naturals));
                    } catch (final ParseException exception) {
                        cli.printf("[error] %s\n", exception.getMessage());
                    } catch (final Throwable throwable) {
                        cli.printf("[error] %s\n", throwable.getMessage());
                        throwable.printStackTrace();
                    }
                }
            }

        };

        cli = new CommandLineInterface("\u03BB>") {

            @Override
            public void accept(final String input) throws IOException {
                if (Debugger.isDebugging()) {
                    final String line = input.trim();

                    for (final Command command : Debugger.commands) {
                        try {
                            if (command.parse(line)) {
                                return;
                            }
                        } catch (final CommandException exception) {
                            cli.printf("[error] %s\n", exception.getMessage());
                            return;
                        }
                    }

                    cli.printf("[error] Unknown debugger command with name '%s' (try 'help' for more information)\n", line.split("\\s+")[0]);
                } else if (input.trim().startsWith(":")) {
                    final String line = input.trim().substring(1).trim();

                    for (final Command command : commands) {
                        try {
                            if (command.parse(line)) {
                                return;
                            }
                        } catch (final CommandException exception) {
                            cli.printf("[error] %s\n", exception.getMessage());
                            return;
                        } catch (final IOException exception) {
                            throw exception;
                        } catch (final Throwable throwable) {
                            cli.printf("[error] %s\n", throwable.getMessage());
                            throwable.printStackTrace();
                            return;
                        }
                    }

                    cli.printf("[error] Unknown command with name '%s' (try ':help' for more information)\n", line.split("\\s+")[0]);
                } else {
                    try {
                        Term term = bind(Parser.parse(input));

                        while (!strategy.isNormal(term)) {
                            term = strategy.reduce(term);
                        }

                        cli.printf("%s\n", term.show(naturals));
                    } catch (final ParseException exception) {
                        cli.printf("[error] %s\n", exception.getMessage());
                    } catch (final Throwable throwable) {
                        cli.printf("[error] %s\n", throwable.getMessage());
                        throwable.printStackTrace();
                    }
                }
            }

        };

        cli.printf("""
                 
                Welcome to the ...
                           
                 _                        _          _       \s
                | |                      | |        | |      \s
                | |      __ _  _ __ ___  | |__    __| |  __ _\s
                | |     / _` || '_ ` _ \\ | '_ \\  / _` | / _` |
                | |____| (_| || | | | | || |_) || (_| || (_| |
                \\_____/ \\__,_||_| |_| |_||_.__/  \\__,_| \\__,_|
                            
                                     ______
                                    |______|
                     
                 _____         _               _            \s
                /  __ \\       | |             | |           \s
                | /  \\/  __ _ | |  ___  _   _ | | _   _  ___\s
                | |     / _` || | / __|| | | || || | | |/ __|
                | \\__/\\| (_| || || (__ | |_| || || |_| |\\__ \\
                 \\____/ \\__,_||_| \\___| \\__,_||_| \\__,_||___/
                 
                
                You are using version %s.
                
                Visit https://git.thm.de/bpfr67/lambda-calculus to check
                if you have the most recent version.
                
                Have fun (type ':help' if you do not know where to start)!
                
                """, version);
        cli.prompt();
    }

}
